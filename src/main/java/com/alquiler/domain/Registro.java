package com.alquiler.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;


@Setter
@Getter
@Entity
public class Registro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private LocalDate fechaDia;
    private String placa;
    private String comprador;
    private String telComprador;
    private String direccion;
    private String email;
    private String docIdentidad;
    private LocalDate fechaFinal;
    private Number precio;

}
