package com.alquiler.service.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class RegistroDTO {

    @NonNull
    private int id;
    private LocalDate fechaDia;
    private String placa;
    private String comprador;
    private String telComprador;
    private String direccion;
    private String email;
    private String docIdentidad;
    private LocalDate fechaFinal;
    private Number precio;


}
