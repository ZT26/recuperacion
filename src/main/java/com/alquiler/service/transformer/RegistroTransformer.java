package com.alquiler.service.transformer;

import com.alquiler.domain.Registro;
import com.alquiler.service.dto.RegistroDTO;

import java.time.LocalDate;

public class RegistroTransformer {

    public static RegistroDTO getRegistroDTOFromRegistro(Registro registro){
        if (registro == null){
            return null;
        }

        RegistroDTO dto = new RegistroDTO();

        dto.setId(registro.getId());
        dto.setFechaDia(registro.getFechaDia());
        dto.setPlaca(registro.getPlaca());
        dto.setComprador(registro.getComprador());
        dto.setTelComprador(registro.getTelComprador());
        dto.setDireccion(registro.getDireccion());
        dto.setEmail(registro.getDireccion());
        dto.setDocIdentidad(registro.getDocIdentidad());
        dto.setFechaFinal(registro.getFechaFinal());
        dto.setPrecio(registro.getPrecio());

        return dto;

    }

    public static Registro getRegistroFromRegistroDTO(RegistroDTO dto){
        if (dto == null){
            return  null;
        }
        Registro registro = new Registro();

        registro.setId(registro.getId());
        registro.setFechaDia(registro.getFechaDia());
        registro.setPlaca(registro.getPlaca());
        registro.setComprador(registro.getComprador());
        registro.setTelComprador(registro.getTelComprador());
        registro.setDireccion(registro.getDireccion());
        registro.setEmail(registro.getDireccion());
        registro.setDocIdentidad(registro.getDocIdentidad());
        registro.setFechaFinal(registro.getFechaFinal());
        registro.setPrecio(registro.getPrecio());

        return registro;

}

}