package com.alquiler.service;

import com.alquiler.domain.Registro;
import com.alquiler.repository.RegistroRepository;
import com.alquiler.service.dto.RegistroDTO;
import com.alquiler.service.transformer.RegistroTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IRegistroServiceImp implements IRegistroService {

    @Autowired
    RegistroRepository registroRepository;


    @Override
    public RegistroDTO create(RegistroDTO registroDTO) {
        Registro registro =
        RegistroTransformer.getRegistroFromRegistroDTO(registroDTO);
        return (RegistroTransformer.getRegistroDTOFromRegistro(registroRepository.save(registro)));
    }

    @Override
    public Page<RegistroDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable= PageRequest.of(pageSize, pageNumber);
        return registroRepository.findAll(pageable)
                .map(RegistroTransformer::getRegistroDTOFromRegistro);
    }

}
