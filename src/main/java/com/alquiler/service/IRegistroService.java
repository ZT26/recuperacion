package com.alquiler.service;


import com.alquiler.service.dto.RegistroDTO;
import org.springframework.data.domain.Page;


public interface IRegistroService {

    public RegistroDTO create(RegistroDTO registroDTO);

    public Page<RegistroDTO> read(Integer pageSize, Integer pageNumber);


}
