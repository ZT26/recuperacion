package com.alquiler.repository;

import com.alquiler.domain.Registro;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RegistroRepository extends JpaRepository<Registro, Integer> {


}