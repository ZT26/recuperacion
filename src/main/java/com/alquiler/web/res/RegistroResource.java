package com.alquiler.web.res;


import com.alquiler.service.IRegistroService;
import com.alquiler.service.dto.RegistroDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")

public class RegistroResource {
    @Autowired
    IRegistroService registroService;

    @PostMapping("/registro")
    public RegistroDTO create(@RequestBody RegistroDTO registroDTO){

        return registroService.create(registroDTO);
    }

    @GetMapping("/registro")
    public Page<RegistroDTO> read(@RequestParam("pageSize") Integer pageSize,
                                  @RequestParam("pageNumber") Integer pageNumber) {
    return registroService.read(pageSize, pageNumber);
    }

}
